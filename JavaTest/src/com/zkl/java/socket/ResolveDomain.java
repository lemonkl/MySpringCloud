package com.zkl.java.socket;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;
/**
 * IP地址DNS服务器解析域名
 * 
 * @author SUN
 *
 */
public class ResolveDomain {
    
    
    public static void main(String[] args) throws UnknownHostException {  
        ResolveDomain rd = new ResolveDomain();
        rd.resolveDomain("www.baidu.com", "223.5.5.5", new ConcurrentLinkedQueue<String>());
    }  

    public void resolveDomain(String domain, String DNS, ConcurrentLinkedQueue<String> queue){
        System.setProperty("sun.net.spi.nameservice.provider.1", "dns,sun");
        System.setProperty("sun.net.spi.nameservice.nameservers", DNS);        
        InetAddress[] addresses;
        try {
            addresses = InetAddress.getAllByName(domain);    //IP or domain
            for (int i = 0; i < addresses.length; i++) {
                String ip = addresses[i].getHostAddress();
                System.out.println(DNS + "\t" + domain + "\t" + ip);
                queue.add(DNS + "\t" + domain + "\t" + ip);
            } 
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        
    }
}
/* 114DNS
 * "www.baidu.com", "114.114.114.114"
 * 114.114.114.114  www.baidu.com   61.135.169.125
 * 114.114.114.114 www.baidu.com   61.135.169.121
 * 
 * 百度DNS
 * 223.5.5.5
 * 223.5.5.5    www.baidu.com   111.206.223.206
   223.5.5.5   www.baidu.com   111.206.223.205

 * */
