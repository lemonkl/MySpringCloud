package com.zkl.java.thread;

/**
 * 实现Runnable接口
 */
public class MyRunnable implements Runnable {

    private int i = 0;

    @Override
    public void run() {
        for (; i < 100; i++) {
            // 当线程类实现Runnable接口时
            // 如果想获取当前线程，只能是Thread.currentThread()的方法
            System.out.println(Thread.currentThread().getName() + " " + i);

        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            // 调用Thread的currenThread()方法获取当前线程
            System.out.println(Thread.currentThread().getName() + " " + i);

            if (i == 20) {
                MyRunnable sc = new MyRunnable();
                // 创建第一个线程
                new Thread(sc, "第一个线程").start();
                // 创建第二个线程
                new Thread(sc, "第二个线程").start();
            }
        }
    }
}
