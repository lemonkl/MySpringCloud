package com.zkl.java.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 使用Callable和Future创建线程
 */
public class MyCallable {

    public static void main(String[] args) {
        //创建Callable对象
        //先使用Lambda表达式创建Callable<Integer>对象
        //使用FutureTask来包装Callable<Integer>对象
        FutureTask<Integer> task = new FutureTask<Integer>((Callable<Integer>) () -> {
            int i = 0;
            for (; i < 10; i++) {
                System.out.println(Thread.currentThread().getName());
            }
            return i;//call()方法可以有返回值
        });
        
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName() + "循环变量" + i + "的值");
            if (i == 20) {
                //实质还是以Callable对象来创建并启动线程的
                new Thread(task, "返回值的线程").start();
            }
        }
        try {
            //创建线程返回值
            System.out.println("子线程返回值：" + task.get());
            System.err.println("执行完成");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

}
