package com.zkl.java.thread;

/**
 * 通过继承Thread类来创建线程类
 */
public class MyThread extends Thread {

    private int i = 0;

    @Override
    public void run() {
        for (; i < 100; i++) {
            // 当线程类继承Thread类时，直接使用this即可获取当前线程
            // Thread对象的getName()返回当前线程的名字
            // 因此可以直接调用getName()方法返回当前线程的名字
            System.out.println(getName() + " " + i);

        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            // 调用Thread的currenThread()方法获取当前线程
            System.out.println(Thread.currentThread().getName() + " " + i);
            
            if(i == 20) {
                //创建第一个线程
                new MyThread().start();
                //创建第二个线程
                new MyThread().start();
            }
        }
    }
}
